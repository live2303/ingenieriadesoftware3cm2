create table Sequelizemeta(
Name varchar(255) not null primary key,
unique(Name)
);

create table Responsables(
idResponsable int(11) not null auto_increment primary key,
nombre varchar(50) not null,
Puesto varchar(25) not null,
Extension varchar(7) not null,
Celular varchar(10) not null,
correoInstitucional varchar(45) not null,
correoAlternativo varchar(45) not null,
tipo varchar(1),
createAt datetime not null,
updateAt datetime not null
);

create table planEstudios(
idplanEstudios int(11) not null auto_increment primary key,
version varchar(10) not null,
createAt datetime not null,
updateAt datetime not null
);

create table programasAcademicos(
idprogramaAcademico int(11) not null auto_increment primary key,
Nombre varchar(50) not null,
idplanEstudios int(11) not null,
foreign key(idplanEstudios) references planEstudios(idplanEstudios)
on update cascade on delete cascade,
createAt datetime not null,
updateAt datetime not null
);

create table unidadesAprendizaje(
idunidadAprendizaje int(11) not null auto_increment primary key,
Nombre varchar(50),
idplanEstudios int(11) not null,
foreign key(idplanEstudios) references planEstudios(idplanEstudios)
on update cascade on delete cascade,
createAt datetime not null,
updateAt datetime not null
);

create table unidadesAcademicas(
idunidadAcademica int not null auto_increment primary key,
Nombre varchar(50),
createAt datetime not null,
updateAt datetime not null
);

create table Solicitudes(
idSolicitud int(11) not null auto_increment primary key,
FechaRecepcion datetime not null,
Descripcion text not null,
idunidadAcademica int(11) not null,
foreign key(idunidadAcademica) references unidadesAcademicas(idunidadAcademica)
on update cascade on delete cascade,
idunidadAprendizaje int(11) not null,
foreign key(idunidadAprendizaje) references unidadesAprendizaje(idunidadAprendizaje)
on update cascade on delete cascade,
createAt datetime not null,
updateAt datetime not null
); 

create table Res_Sol(
idResponsable int(11) not null,
foreign key(idResponsable) references Responsables(idResponsable)
on update cascade on delete cascade,
idSolicitud int(11) not null,
foreign key(idSolicitud) references Solicitudes(idSolicitud)
on update cascade on delete cascade,
createAt datetime not null,
updateAt datetime not null
);