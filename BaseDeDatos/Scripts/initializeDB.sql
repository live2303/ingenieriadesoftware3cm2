insert into sequelizemeta values("20190414005354-create-unidad-academica.js");
insert into sequelizemeta values("20190414005354-create-ud-aprendizaje.js");
insert into sequelizemeta values("20190414005354-create-solicitud.js");
insert into sequelizemeta values("20190414005354-create-programa-academico.js");

insert into Responsables (Nombre,Puesto,Extension,Celular,correoInstitucional,correoAlternativo,tipo,createAt,updateAt) values ("Diego Armando","Subdirector Academico","0454367","5565793245","ProfArmando@ipn.com.mx","DArmando@gmail.com","0","2019-04-14","2019-04-14");
insert into Responsables (Nombre,Puesto,Extension,Celular,correoInstitucional,correoAlternativo,tipo,createAt,updateAt) values ("Enrique Guzman","Subgerente","0434354","5565793245","Enrq@ipn.com.mx","Enrique@gmail.com","1","2019-05-09","2019-07-10");
insert into Responsables (Nombre,Puesto,Extension,Celular,correoInstitucional,correoAlternativo,tipo,createAt,updateAt) values ("Carlos Aguilar","Director Academico","0253567","5565793245","CarlosAg@ipn.com.mx","Carlos@gmail.com","0","2019-07-22","2019-10-22");
insert into Responsables (Nombre,Puesto,Extension,Celular,correoInstitucional,correoAlternativo,tipo,createAt,updateAt) values ("Lalo Sanchez","Pasante","1454345","5565793245","LaloSanchez@ipn.com.mx","LaloLalo@gmail.com","1","2019-09-20","2019-11-05");

insert into unidadesAcademicas (Nombre,createAt,updateAt) values ("Cecyt No.9","2019-01-14","2019-04-10");
insert into unidadesAcademicas (Nombre,createAt,updateAt) values ("ESCOM","2019-08-14","2019-10-11");
insert into unidadesAcademicas (Nombre,createAt,updateAt) values ("Cecyt No.12","2019-10-14","2019-04-14");
insert into unidadesAcademicas (Nombre,createAt,updateAt) values ("UPITA","2019-04-14","2019-11-14");
insert into unidadesAcademicas (Nombre,createAt,updateAt) values ("Cecyt No.3","2019-02-14","2019-04-14");

insert into planEstudios (Version,createAt,updateAt) values ("0001-0002a","2019-04-14","2019-06-14");
insert into planEstudios (Version,createAt,updateAt) values ("0002-0002b","2019-04-14","2019-07-14");
insert into planEstudios (Version,createAt,updateAt) values ("0002-0002b","2019-04-14","2019-09-14");
insert into planEstudios (Version,createAt,updateAt) values ("0002-0002c","2019-04-14","2019-010-14");

insert into programasAcademicos (Nombre,idplanEstudios,createAt,updateAt) values ("ISC",1,"2019-02-10","2019-04-14");
insert into programasAcademicos (Nombre,idplanEstudios,createAt,updateAt) values ("Telematica",2,"2019-02-15","2019-04-16");
insert into programasAcademicos (Nombre,idplanEstudios,createAt,updateAt) values ("ISC",3,"2019-03-09","2019-04-24");
insert into programasAcademicos (Nombre,idplanEstudios,createAt,updateAt) values ("Telematica",4,"2019-04-01","2019-04-14");

insert into unidadesAprendizaje (Nombre,idplanEstudios,createAt,updateAt) values ("Inenieria De Software",1,"2019-01-14","2019-04-10");
insert into unidadesAprendizaje (Nombre,idplanEstudios,createAt,updateAt) values ("Calculo Aplicado",2,"2019-01-14","2019-04-10");
insert into unidadesAprendizaje (Nombre,idplanEstudios,createAt,updateAt) values ("Mineria de Datos",3,"2019-01-14","2019-04-10");
insert into unidadesAprendizaje (Nombre,idplanEstudios,createAt,updateAt) values ("IA",4,"2019-01-14","2019-04-10");

insert into Solicitudes (FechaRecepcion,Descripcion,idUnidadAcademica,idunidadAprendizaje,createAt,updateAt) values ("2019-01-12","Solicitud resivida en tiempo y forma",1,1,"2019-01-15","2019-01-20");
insert into Solicitudes (FechaRecepcion,Descripcion,idUnidadAcademica,idunidadAprendizaje,createAt,updateAt) values ("2019-01-15","Solicitud de correcciones",2,2,"2019-01-17","2019-01-18");
insert into Solicitudes (FechaRecepcion,Descripcion,idUnidadAcademica,idunidadAprendizaje,createAt,updateAt) values ("2019-02-10","Solicitud resivida en tiempo y forma",3,3,"2019-02-20","2019-02-20");
insert into Solicitudes (FechaRecepcion,Descripcion,idUnidadAcademica,idunidadAprendizaje,createAt,updateAt) values ("2019-03-12","Solicitud de correcciones",4,4,"2019-03-21","2019-03-21");

insert into Res_Sol (idResponsable,idSolicitud,createAt,updateAt) values (1,1,"2019-01-15","2019-01-20");
insert into Res_Sol (idResponsable,idSolicitud,createAt,updateAt) values (2,2,"2019-01-17","2019-01-18");
insert into Res_Sol (idResponsable,idSolicitud,createAt,updateAt) values (3,4,"2019-02-20","2019-02-20");
insert into Res_Sol (idResponsable,idSolicitud,createAt,updateAt) values (4,3,"2019-03-21","2019-03-21");