ISW - Domingo, 7 de abril de 2019.
Los casos de uso detectados, son los siguientes.

Escuela.
- Añadir Escuela.
- Consultar Escuela.
- Modificar Escuela.

Participante.
- Añadir participante.
- Consultar participante.
- Modificar participante.
- Dar de baja participante.

Responsable.
- Añadir responsable.
- Consultar responsable.
- Modificar responsable.
- Dar de baja responsable.

Receptor.
- Añadir receptor.
- Consultar receptor.
- Modificar receptor.
- Dar de baja receptor.

Solicitud.
- Añadir solicitud.
- Consultar solicitud.
- Dar de baja solicitud.

UAL.
- Añadir UAL.
- Consultar UAL.
- Modificar UAL (Duda).
- Dar de baja UAL (Duda).